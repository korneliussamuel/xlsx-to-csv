package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/xuri/excelize/v2"
)

var (
	inputFileName  = flag.String("i", "input.xlsx", "input file")
	outputFileName = flag.String("o", "output.csv", "output file")
	sheetIndex     = flag.Int("si", 0, "sheet index")
	removeTop      = flag.Int("t", 0, "remove n top rows")
	removeBottom   = flag.Int("b", 0, "remove n bottom rows")
)

func main() {
	setupFlags()

	fmt.Println("Converting file:", *inputFileName)
	inputFile, err := excelize.OpenFile(*inputFileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer inputFile.Close()

	outputFile, err := os.Create(*outputFileName)
	if err != nil {
		log.Fatalln("Failed to create output file")
	}
	defer outputFile.Close()
	csvWriter := csv.NewWriter(outputFile)
	defer csvWriter.Flush()

	sheet := inputFile.GetSheetName(*sheetIndex)
	fmt.Println("Scanning rows in sheet:", sheet, "\nThis might take a while...")
	rows, err := inputFile.GetRows(sheet)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Conversion in progress...")
	csvRows := convert(removeTopAndBottom(rows))
	if err := csvWriter.WriteAll(csvRows); err != nil {
		log.Fatalln("Error in writing CSV:", err)
	}
	fmt.Println("Done, output file:", *outputFileName)
}

func setupFlags() {
	flag.Parse()
	if flag.NArg() != 0 {
		log.Fatalln("Invalid NArg count:", flag.NArg())
	}
}

func removeTopAndBottom(rows [][]string) [][]string {
	start := *removeTop
	end := len(rows) - *removeBottom
	return rows[start:end][:]
}

func convert(rows [][]string) [][]string {
	var csvRows [][]string
	for _, row := range rows {
		var csvRow []string
		for _, val := range row {
			val = strings.ReplaceAll(val, "\n", " ")
			val = strings.ReplaceAll(val, ";", "")
			if val == "-" || val == "_" {
				val = ""
			}
			csvRow = append(csvRow, val)
		}
		csvRows = append(csvRows, csvRow)
	}
	return csvRows
}
