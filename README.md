<b>XLSX => CSV file converter</b>

HOW TO USE
- copy binary file (`xlsx-to-csv`) to your local machine
- if the file does not have executable permission, run `chmod +x xlsx-to-csv`
- to execute: `./xlsx-to-csv -i {{path_to_input_file}} -o {{path_output_file}} -t {{remove_top_rows}} -b {{remove_bottom_rows}}`
    - example: `./xlsx-to-csv -i input.xlsx -o output.csv -t 5 -b 2`
    - the app will convert a file name input.xlsx (within the same directory) to output.csv
    - in the conversion, the app will ignore 5 top rows and the last 2 bottom rows from input file

FLAGS:
- `-i` : path_to_input_file, default value; *input.xlsx*
- `-o` : path_to_output_file, default value: *output.xlsx*
- `-t` : remove_top_rows, default value: *0*
- `-b` : remove_bottom_rows, default value: *0*